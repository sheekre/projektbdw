﻿/// Model firmy
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Projekt.Models
{
/// <summary>
/// Model firmy
/// Klasa modelu firmy i jej informacji
/// </summary>

    public class Firmy
    {
        public int IDFirmy { get; set; }
        public string Nazwa { get; set; }
        public string Adres { get; set; }
        public string Miasto { get; set; }
        public string Kod { get; set; }
        public string Bank { get; set; }
        public string Konto { get; set; }

        public int SaveDetails()
        {
            SqlConnection con = new SqlConnection(GetConnectionString.ConString());
            con.ConnectionString = "Data Source=LAPTOP-ASA6V7LH\\SQLEXPRESS;Initial Catalog=BDW;Integrated Security=True";
            string query = "INSERT INTO Firma(Nazwa, Adres, Miasto, Kod, Bank ,Konto) values ('" + Nazwa + "','" + Adres + "'," +
                "'" + Miasto + "','" + Kod + "','" + Bank + "','" + Konto + "')";
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();
            return i;
        }
    }


}
