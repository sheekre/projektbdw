﻿/// Testy jednostkowe
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Projekt.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Projekt.Models.Tests
{
/// <summary>
/// Testy jednostkowe
/// </summary>
    
    [TestClass()]
    public class ZbierzDaneZBazyTests
    {
        /// <summary>
        /// Test Nazw
        /// </summary>
        [TestMethod()]
        public void GetComNamesTest()
        {
            List<string> list = new List<string>();

            list.Add("FHU Olimp s.c.");
            list.Add("Amber s.c.");
            List<bool> score = new List<bool>();
            bool score2;
            ZbierzDaneZBazy zb = new ZbierzDaneZBazy();
            List<ZbierzDaneZBazy> what = new List<ZbierzDaneZBazy>();
            what = zb.GetComNames();
            foreach (var item in what)
            {
                foreach (var item2 in list)
                {
                    if (item2.ToString().Equals(item.Nazwa.ToString()))
                    {
                        score.Add(true);
                    }
                }
            }
            if (!score.Count.Equals(list.Count))
            {
                score2 = false;
            }
            else
            {
                score2 = true;
            }
            Assert.AreEqual(true, score2);
        }

        /// <summary>
        /// Test walut
        /// </summary>
        [TestMethod()]
        public void GedWalutyTest()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("PLN", "Polski Złoty");
            list.Add("EUR", "Euro");
            list.Add("USD", "Dolar Amerykański");
            List<bool> score = new List<bool>();
            bool score2;
            ZbierzDaneZBazy zb = new ZbierzDaneZBazy();
            List<ZbierzDaneZBazy> what = new List<ZbierzDaneZBazy>();
            what = zb.GedWaluty();
            foreach (var item in what)
            {
                foreach (var item2 in list)
                {
                    if (item2.Key.ToString().Equals(item.SymbolW.ToString()) && item2.Value.ToString().Equals(item.NazwaW.ToString()))
                    {
                        score.Add(true);
                    }
                }
            }
            if (!score.Count.Equals(list.Count))
            {
                score2 = false;
            }
            else
            {
                score2 = true;
            }
            Assert.AreEqual(true, score2);
        }

        /// <summary>
        /// Test firm
        /// </summary>
        [TestMethod()]
        public void GetIDFirmyTest()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("FHU Olimp s.c.", "1");
            list.Add("Amber s.c.", "4");
            List<bool> score = new List<bool>();
            bool score2 = false;
            ZbierzDaneZBazy zb = new ZbierzDaneZBazy();
            int idFirmy;
            foreach (var itemz in list)
            {
                idFirmy = zb.GetIDFirmy(itemz.Key.ToString());
                foreach (var item2 in list)
                {
                    if (item2.Value.ToString().Equals(idFirmy.ToString()))
                    {
                        score.Add(true);
                    }
                }
            }
            if (!score.Count.Equals(list.Count))
            {
                score2 = false;
            }
            else
            {
                score2 = true;
            }
            Assert.AreEqual(true, score2);
        }
        }
}